#!/usr/bin/lua
-- vim:fdm=marker ts=4
-- gfwlist update file for openwrt
-- put this file to /www/cgi-bin/gfwlist and chmod +x it
-- and you can get pacfile with http://...openwrtip.../cgi-bin/gfwlist
--config {{{
config={
	gfwlist_cache_file="/var/run/gfwlist_cache.lua",
	expires=6*3600,
	adp_file="/etc/tinyady.domain",
	dnsmasq={
--		v4dnsserver="127.0.0.1#1053",
		config_file="/etc/dnsmasq.d/gfwlist.conf",
		ipset="gfwlist",
		restart=true
	},
	builtin={
		"blogspot.com",
		"blogspot.jp"
	}
}
---}}}
	
--{{{base64_decode
local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/' 
function base64_decode(data)
	data = string.gsub(data, '[^'..b..'=]', '')
	return (data:gsub('.', function(x)
		if (x == '=') then return '' end
		local r,f='',(b:find(x)-1)
		for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
		return r;
	end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
	if (#x ~= 8) then return '' end
	local c=0
	for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
	return string.char(c)
	end))
end
---}}}
function update(gfwlist) --{{{
	local gfwlist_url='https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt'
	local fd = io.popen("wget -q --no-check-certificate -O - " .. gfwlist_url)
	local rule= fd:read("*all")
	fd:close()
	if type(rule) ~= "string" or #rule < 4096 then
		return
	end
	rule=base64_decode(rule)
	local domains={}
	for line in string.gmatch(rule, "(.-)\r?\n") do
		if line ~= "" and
			string.sub(line, 1,1) ~= "[" and 
			string.sub(line, 1, 1) ~= "!" and
			string.sub(line, 1, 1) ~= "/" and
			string.sub(line, 1,2) ~= "@@" then
			if string.sub(line, 1,2) == "||" then line=string.sub(line, 3)end
			if string.sub(line, 1,1) == "|" then line=string.sub(line, 2)end
			line = string.gsub(line, "^https?://", "")
			if string.sub(line, 1,1) == "." then line=string.sub(line, 2)end
			--line = string.gsub(line, "^%.", "")
			line = string.gsub(line, "/.*", "")
			line = string.gsub(line, ":.*", "")
			line = string.gsub(line, "%*.*", "")
			line = string.gsub(line, "%%.*", "")
			line = string.gsub(line, "%W+$", "")
			--table.insert(domains, line)
			if string.find(line, "%.") then
				table.insert(domains, line)
			end
		end
	end
	local google_suffix= {"ac","ad","ae","al","am","as","at","az","ba","be","bf","bg","bi","bj","bs","bt","by","ca","cat","cd","cf","cg","ch","ci","cl","cm","co.ao","co.bw","co.ck","co.cr","co.id","co.il","co.in","co.jp","co.ke","co.kr","co.ls","co.ma","com","com.af","com.ag","com.ai","com.ar","com.au","com.bd","com.bh","com.bn","com.bo","com.br","com.bz","com.co","com.cu","com.cy","com.do","com.ec","com.eg","com.et","com.fj","com.gh","com.gi","com.gt","com.hk","com.jm","com.kh","com.kw","com.lb","com.ly","com.mm","com.mt","com.mx","com.my","com.na","com.nf","com.ng","com.ni","com.np","com.om","com.pa","com.pe","com.pg","com.ph","com.pk","com.pr","com.py","com.qa","com.sa","com.sb","com.sg","com.sl","com.sv","com.tj","com.tr","com.tw","com.ua","com.uy","com.vc","com.vn","co.mz","co.nz","co.th","co.tz","co.ug","co.uk","co.uz","co.ve","co.vi","co.za","co.zm","co.zw","cv","cz","de","dj","dk","dm","dz","ee","es","fi","fm","fr","ga","ge","gg","gl","gm","gp","gr","gy","hk","hn","hr","ht","hu","ie","im","iq","is","it","je","jo","jp","kg","ki","kz","la","li","lk","lt","lu","lv","md","me","mg","mk","ml","mn","ms","mu","mv","mw","mx","ne","nl","no","nr","nu","org","pl","pn","ps","pt","ro","rs","ru","rw","sc","se","sh","si","sk","sm","sn","so","sr","st","td","tg","tk","tl","tm","tn","to","tt","vg","vu","ws"}
	for i,v in ipairs(google_suffix) do
		table.insert(domains, "google." .. v)
	end
	google_suffix = nil

	for i,v in ipairs(config.builtin) do
		table.insert(domains, v)
	end
	
	table.sort(domains, function (a, b)
		return string.len(a) < string.len(b)
	end)

	local result = {}
	for i,v in pairs(domains) do
		domain = v
		while domain do
			if result[domain] then
				break;
			end
			index1 = string.find(domain, "%.")
			if index1 then
				index2 = string.find(domain, "%.", index1 + 1)
				if index2 then
					domain = string.sub(domain, index1+1)
				else
					result[v] = true
					break
				end
			else
				break
			end
		end
	end
	domains = {}
	for i,v in pairs(result) do
		table.insert(domains, i)
	end
	result = nil
	gfwlist.domains=domains
	gfwlist.version=os.time()
	return 
end
---}}}
function dump(gfwlist, dumpfile) --{{{
	local f = assert(io.open(dumpfile, "w"))
	f:write("gfwlist={\n\tversion=", gfwlist.version, ",\n\tdomains={\n")
	for i,v in ipairs(gfwlist.domains) do
		if i==1 then
			f:write('\t\t"', v)
		else
			f:write('",\n\t\t"', v)
		end
	end
	f:write('"\n\t}\n}')
	f:close()
end
--}}}
--生成pac {{{
local pac_template=[[
}
var proxy="SOCKS5 192.168.1.1:1080";
var direct = "DIRECT;";
var hasOwnProperty = Object.hasOwnProperty;

function FindProxyForURL(url, host) {
	var suffix;
	var pos = host.lastIndexOf('.');

	for (;;) {
		suffix = host.substring(pos + 1);
		if (hasOwnProperty.call(domains, suffix)) {
			return proxy;
		}
		if (pos <= 0) {
			return direct;
		}
		pos = host.lastIndexOf('.', pos - 1);
	}
}
]]
function pac(gfwlist)
	for i,v in ipairs(gfwlist.domains) do
		if i==1 then
			io.write('var domains={\n\t"', v, '":1')
		else
			io.write(',\n\t"', v, '":1')
		end
	end
	io.write("\n", pac_template)
end
---}}}
function adp(gfwlist, adpfile) --{{{
	local f = assert(io.open(adpfile, "w"))
	for i,v in ipairs(gfwlist.domains) do
		f:write("||", v, "\n")
	end
	f:close()
end
--}}}
function dnsmasq(gfwlist, dnsmasq_config) --{{{
	v4dnsserver = dnsmasq_config.v4dnsserver or "208.67.222.222#443"
	dnsfile = dnsmasq_config.config_file or "/etc/dnsmasq.d/gfwlist.conf"
	ipset = dnsmasq_config.ipset
	local f = assert(io.open(dnsfile, "w"))
	if ipset then
		os.execute("ipset -F " .. ipset)
	end
	for i,v in ipairs(gfwlist.domains) do
		if string.match(v, "^%d+%.%d+%.%d+%.%d+$") then
			if ipset then
				os.execute("ipset -A " .. ipset .. " " .. v)
			end
		else
			f:write("server=/.", v, "/", v4dnsserver, "\n")
			if ipset then
				f:write("ipset=/.", v, "/", ipset, "\n")
			end
		end
	end
	f:close()
	if dnsmasq_config.restart then
		os.execute("/etc/init.d/dnsmasq restart")
	end
end
--}}}

gfwlist={}
local x=loadfile(config.gfwlist_cache_file)
if x then
	x()
end
if type(gfwlist.version) ~= "number" or os.difftime(os.time(), gfwlist.version) > config.expires then
	update(gfwlist)
	dump(gfwlist, config.gfwlist_cache_file)
	if arg and arg[1] then
		if config.adp_file then
			adp(gfwlist, config.adp_file)
		end
		if config.dnsmasq then
			dnsmasq(gfwlist, config.dnsmasq)
		end
	end
end

if not (arg and arg[1]) then
	--CGI header
	io.write("Content-Type: text/plain\r\n\r\n")
	pac(gfwlist)
end
	

