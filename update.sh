#!/bin/sh

proxy='SOCKS5 localhost:1080;PROXY localhost:8080; DIRECT;'
direct='DIRECT;'
gfwlist_url="https://raw.githubusercontent.com/gfwlist/gfwlist/master/gfwlist.txt"

cd `dirname $0`

temp_file=$(mktemp)
#wget -q -O $temp_file --no-check-certificate $gfwlist_url || exit 1
wget -q -O $temp_file $gfwlist_url || exit 1
domain_file=$(mktemp)
base64 -d < $temp_file > $domain_file || exit 2
cat $domain_file builtin.txt | grep -v -E '^$|[!\[$]' > $temp_file

sed -e '/^@@/d' -e 's/^|\{1,2\}//' -e 's#^https\{0,1\}://##' -e 's/^\.//' \
  -e 's#/.*##' -e 's#\(\..*\)\*.*#\1#' -e 's#\(\..*\)\*.*#\1#' -e 's#^\(.*\*[^\.]*\.\)##' \
  -e '/^[^\.]*$/d' $temp_file | sort -u \
  | awk '{ print length, $0 }' | sort -n -s | cut -d" " -f2- \
  > $domain_file

echo -e "var proxy=\"$proxy\";\nvar domains={" > $temp_file
awk -F . ' { domain = $(NF); for(i=NF-1;i>0;i--){ domain = $i"."domain; if(domain in domains){ break; } } if(i == 0){ domains[$0] = 1; } } END{ for(i in domains){ printf "\t\"%s\":1,\n", i; } }' $domain_file >> $temp_file 

sed -i '$s/\,$/\n};/' $temp_file
echo -e "var direct = \"$direct\";\nvar hasOwnProperty = Object.hasOwnProperty;\n" >> $temp_file
echo -e "function FindProxyForURL(url, host) {\n\tvar suffix;\n\tvar pos = host.lastIndexOf('.');\n" >> $temp_file
echo -e "\tfor (;;) {\n\t\tsuffix = host.substring(pos + 1);\n\t\tif (hasOwnProperty.call(domains, suffix)) {" >> $temp_file
echo -e "\t\t\treturn proxy;\n\t\t }\n\t\tif (pos <= 0) {\n\t\t\treturn direct;\n\t\t}" >> $temp_file
echo -e "\t\tpos = host.lastIndexOf('.', pos - 1);\n\t}\n}\n" >> $temp_file
chmod +r $temp_file
mv $temp_file proxy.pac 
mv $domain_file domain.txt
#rm $domain_file

#git commit -a -m "update"
#git push
